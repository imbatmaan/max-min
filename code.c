#include <stdio.h>

// MY FUNCTIONS

void createSets(float startVal, float intervalNum, float interval, int arrSize, int a[])
{

    printf("Totally %d samples were input \n", arrSize);
    for (int i = 0; i < intervalNum; i++)
    {
        int y = 0;
        for (int i = 0; i < arrSize; i++)
        {
            if (a[i] >= startVal && a[i] <= startVal + interval)
            {
                y = y + 1;
            }
        }
        printf("%d-th interval (%.3f <= x < %.3f): %d \n", i + 1, startVal, startVal + interval, y);
        startVal = startVal + interval;
    }
}

void minMax(int a[], int n, float divInt)
{
    int min, max, i;
    float interval;
    min = max = a[0];
    for (i = 1; i < n; i++)
    {
        if (min > a[i])
            min = a[i];
        if (max < a[i])
            max = a[i];
    }

    max = max + 1;

    interval = (max - min) / divInt;

    createSets(min, divInt, interval, n, a);
}

// Main()

int main()
{
    int values[10000];
    float divNum;
    int valSize;

    printf("Enter Values: ");

    // taking input and storing it in an array
    for (int i = 0; i < 10000; ++i)
    {
        int a;
        scanf("%d", &a);

        if (a >= 0)
        {
            values[i] = a;
        }
        else
        {
            // Terminating the process because negative value was entered.
            printf("Terminated Because of negative Value \n");
            valSize = i;
            break;
        }
    }

    printf("Displaying integers: \n");

    for (int i = 0; i < valSize; ++i)
    {
        printf("Input %d-th data : %d\n", i + 1, values[i]);
    }

    printf("Input division number : ");
    scanf("%f", &divNum);
    minMax(values, valSize, divNum);

    return 0;
}
